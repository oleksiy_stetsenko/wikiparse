'use strict';

function PagesService($http, utils, ParamService, $window,$q) {

    var self = this;
	self.results = null;
	self.exactMatch = null;
    self.showLoadMore = true;

    /*** METHODS ***/

	self.search = function(callback) {
        var result = [];
        self.clearResults();
        if(!ParamService.getSearchTerm()) return;

        var sources = ParamService.settings.sources;
        var promises = [];

        if (sources) {
            sources.forEach(function (source) {
                var paramUrl = ParamService.createParamUrl(ParamService.getPagesParams(), source);
                var promise = $http.jsonp(paramUrl);
                promises.push(promise);
            });
        }

        $q.all(promises)
            .then(getAllPages)
            .then(function (item) {
                self.results = item;
                if (item.length > 0) {
                    self.download(item);
                }
            });
    }; // search

    function getAllPages(res) {
        var result = [];
        res.forEach(function (course) {
            if (course.data.query) {
                result = result.concat(course.data.query.pages);
                result.sort(function(a, b) {
                    return a.extract - b.extract;
                });
            }
        });
        return result;
    }


    self.download = function (data) {
        var promises = [];
        data.forEach(function (source) {
            var url = source.fullurl.substring(0, source.fullurl.indexOf('org')+3);
            var apiUrl = url + "/w/api.php";
            var getPageParams = ParamService.getPageParams();
            getPageParams.titles = source.title;

            var paramUrl = apiUrl + "?" + utils.serialize(getPageParams);

            var promise = $http.jsonp(paramUrl);
            promises.push(promise);
        });

        function createDownloadContent(data) {
            var newArr = [];
                data.forEach(function (course) {
                    newArr = newArr.concat(course.data.query.pages);
                });
            return newArr;
        }

        $q.all(promises).then(createDownloadContent).then(function (item) {
            if (item && item.length > 0) {
                var content = getContent(item);
                var blob = new Blob([content], { type: 'text/plain' }),
                    url = $window.URL || $window.webkitURL;
                self.fileUrl = url.createObjectURL(blob);
            }
        });

     };

    self.loadMore = function () {
        var paramUrl = ParamService.createParamUrl(ParamService.getPagesParams());
		$http.jsonp(paramUrl)
			.success(function (data) {
                self.toggleLoadMore(Boolean(data.continue));
                if (data.continue) ParamService.setOffset(data.continue.gsroffset);
                if (!data.query) return;
                self.results = self.results.concat(data.query.pages);
			});
    };  // loadMore


    self.clearResults = function() {
        resetErrors();
        self.results = null;
        self.noResultsMessage = null;
        self.exactMatch = null;
        ParamService.resetOffset();
    }; // clearResults


    self.toggleLoadMore = function(bool) {
        self.showLoadMore = bool;
    };


    /*** HELPERS ***/

    function findExactTerm(results){
		var capitalizedTerm = utils.capitalize(ParamService.getSearchTerm());
		var found = null;
		angular.forEach(results, function(result) {
			if (capitalizedTerm == utils.capitalize(result.title)) found = result.title;
			for(var r in result.redirects) {
				if(capitalizedTerm == utils.capitalize(result.redirects[r].title) ) {
					found = found || result.title;
				}
			}
		});
		return found;
	}	// findExactTerm

    function handleErrors(data, status) {
        if(status == 404) {
            self.error = "The domain you requesting does not exist. Try again with different criteria.";
            return;
        }
		self.error = "Oh no, there was some error in geting data: " + status;
	} // handleErrors

    function noResults() {
        self.noResultsMessage = utils.noResultsMessage;
    }

    function resetErrors() {
		self.error = "";
	}	// resetErrors

    function getContent(data) {
        var content = "<!DOCTYPE html><html><head><meta charset=\"utf-8\"><title>Search:" +
            ParamService.getSearchTerm() +"</title>" +
            "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">" +
            "</head><body><div class=\"container\">";

        content += "<h1>Search term: " + ParamService.getSearchTerm() + "</h1>";
        content += "<p>Sources:</p>";

        ParamService.settings.sources.forEach(function (source) {
            content += "<p>" + source.url + "</p>";
        });

        data.forEach(function (item) {
            content += "<article><h2><a href=\"" + item.fullurl + "\" target=\"_blank\">" + item.title + "</a></h2>";
            content += "<section>" + item.extract + "</section>";
        });

        content += "</div></body></html>";
        return content;
    }

} // PagesService


module.exports = PagesService;
