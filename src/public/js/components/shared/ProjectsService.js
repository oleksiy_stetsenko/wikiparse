'use strict';


function ProjectsService($http, ParamService) {

    var service = this;
    service.projects = [];

    /*** DATA ***/

    var availableProjects = [];

    $http.get('/source').then(function (item) {
        availableProjects = item.data;
    });

    /*** METHODS ***/

    service.get = function () {
        // console.log(ParamService.languagesUrl);
        $http.jsonp(ParamService.languagesUrl)
            .success(function (data) {
                resetProjects();
                filterProjects(data);
            });
    }; // get

    service.create = function (project) {
        $http.post('/source', project)
            .success(function (data) {
                availableProjects.push(data);
            });
    };

    service.remove = function (project) {
        $http.delete('/source/' + project._id)
            .success(function (data) {
                availableProjects = availableProjects.filter(function (item) {
                    return item._id !== project._id;
                })
            });
    };

    service.edit = function (project) {
        $http.put('/source/' + project.hash, project);
    };

    /*** HELPERS ***/

    function filterProjects() {
        for (var i = 0; i < availableProjects.length; i++) {
            service.projects.push(availableProjects[i]);
        }
    } // filterProjects

    function resetProjects() {
        service.projects = [];
    } // resetProjects

    function isChosenLang(thisLang) {
        return thisLang.code === ParamService.getLang();
    } // isChosenLang


} // ProjectsService

module.exports = ProjectsService;
