function SearchHeadController(ParamService, ProjectsService, LanguagesService, MainService, utils, PagesService, $uibModal) {
	'use strict';

	LanguagesService.get();
	ProjectsService.get();

	var ctrl = this;
	ctrl.paramService = ParamService;
	ctrl.languageService = LanguagesService;
	ctrl.projectService = ProjectsService;
	ctrl.pagesService = PagesService;

	ctrl.selectedProjects = [];

	/*** PUBLIC METHODS ***/

	ctrl.checkMaxResults = function () {
		if (ParamService.pages.gsrlimit > 50) ParamService.pages.gsrlimit = 50;
	}; // checkMaxResults

	ctrl.isSelectedProject = function (project) {
		return ParamService.settings.domain == project.name;
	}; // isChosenProject

	ctrl.toggleSave = function () {
		ParamService.toggleSave();
	}; // toggleRemember

	ctrl.reset = function () {
		resetSearchTerm();
        MainService.clearResults();
	}; // reset

    ctrl.hardReset = function () {
        ctrl.reset();
        ParamService.turnOffRemember();
        ParamService.resetToDefaults();
    }; // reset

	ctrl.refreshLanguages = function () {
		LanguagesService.get();
	}; // refreshLanguages

	ctrl.refreshProjects = function () {
		ProjectsService.get();
	}; // refreshProjects

	ctrl.isCommons = function () {
		return ParamService.isCommons();
	}; // isCommons

	ctrl.updateProject = function(project) {
		// console.log('Saving title ' + project);
		ProjectsService.edit(project);
	};

	ctrl.cancelEdit = function(project) {
		console.log('Canceled editing', project);
	};

	ctrl.removeProject = function (project) {
		ProjectsService.remove(project);
		ProjectsService.get(project);
	};

	ctrl.editProject = function (project) {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'js/components/search-header/modal.html',
			controller: 'ModalInstanceController',
			resolve: {
				project: function () {
					return project;
				}
			}
		});

		modalInstance.result.then(function (project) {
			ProjectsService.edit(project);
			ctrl.proects = ProjectsService.get();
		}, function () {
			console.log('Modal dismissed at: ' + new Date());
		});
	};

	ctrl.addProject = function () {
		var modalInstance = $uibModal.open({
			animation: true,
			templateUrl: 'js/components/search-header/modal.html',
			controller: 'ModalInstanceController',
			resolve: {
				project: function () {
					return {
						name: null,
						url: null
					};
				}
			}
		});

		modalInstance.result.then(function (project) {
			// if (project.url || endWith(project.url, '.wikipeadia.org')) {
			// 	ctrl.showError = true;
             //    hideErrMessage();
			// } else {
				ProjectsService.create(project);
				ProjectsService.get(project);
			// }
		// }, function () {
		// 	console.log('Modal dismissed at: ' + new Date());
		});
	};

    function hideErrMessage() {
        function extracted() {
            ctrl.showError = false;
        }
        setTimeout(extracted, 500);
	}

	ctrl.selectAll = function () {
		if (ctrl.selectedProjects.length > 0) {
			ctrl.selectedProjects = [];
			ctrl.checkAll = false;
			ctrl.projectService.projects.forEach(function (item) {
				item.isChecked = false;
			});
		} else {
			ctrl.checkAll = true;
			ctrl.projectService.projects.forEach(function (item) {
				ctrl.selectedProjects.push(item);
				item.isChecked = true;
			});
		}
		ParamService.settings.sources = ctrl.selectedProjects;
	};

	ctrl.toogleProject = function (project) {
		if (ctrl.checkAll == true) {
			ctrl.checkAll = false
		}

        var contains = ctrl.selectedProjects.some(function (item) {
            return item === project;
        });

        if (!contains) {
            ctrl.selectedProjects.push(project);
        } else {
            var index = ctrl.selectedProjects.indexOf(project);
            ctrl.selectedProjects.splice(index, 1);
        }
        ParamService.settings.sources = ctrl.selectedProjects;
	};

	/*** PRIVATE ***/

	function resetSearchTerm() {
		ParamService.setSearchTerm('');
		utils.resetPath();
	} // resetSearchTerm


} // SearchHeadController

module.exports = SearchHeadController;
