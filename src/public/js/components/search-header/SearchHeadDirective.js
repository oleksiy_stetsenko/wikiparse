'use strict';

function searchHeader() {
	return {
		controller: 'SearchHeadController',
		controllerAs: 'ctrl',
		templateUrl: 'js/components/search-header/search-header.html'
	};
} // searchHeader

module.exports = searchHeader;
