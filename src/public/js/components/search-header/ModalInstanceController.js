function ModalInstanceController($scope, $uibModalInstance, project) {
    var prev = angular.copy(project);
    $scope.project = project;

    $scope.ok = function () {
        $uibModalInstance.close($scope.project);
    };

    $scope.cancel = function () {
        $scope.project.name = prev.name;
        $scope.project.url = prev.url;
        $uibModalInstance.dismiss('cancel');
    };
}

module.exports = ModalInstanceController;