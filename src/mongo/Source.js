var mongoose = require('./mongoose');
var shortid = require('shortid');

var schema = mongoose.Schema({
    hash: {type: String, default: shortid.generate},
    name: 'String',
    url: 'String'
}, {
    collection: 'Source'
});

var Source = mongoose.model('Source', schema);

module.exports = Source;