var express = require('express');
var router = express.Router();
var Source = require('../mongo/Source');

router.route('/')
    .get(function (req, res) {
        Source
            .find()
            .exec(function (err, document) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                res.json(document);
            });
    })
    .delete(function (req, res) {
        Source
            .findOneAndRemove({_id: req.params.hash}, function (err) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                res.sendStatus(200);
            });
    })
    .post(function (req, res) {
        var source = new Source(req.body);
        console.log(req.body.name);
        source.name = req.body.name;
        source.url = req.body.url;
        source.save();
        res.json(source);
    });

router.route('/:id')
    .put(function (req, res) {
        Source
            .findOneAndUpdate({hash: req.params.id}, req.body, function (err, project) {
                if (err) {
                    console.error(err);
                    res.sendStatus(500);
                }
                res.sendStatus(200);
            })
    })
    .delete(function (req, res) {
        Source
            .findByIdAndRemove(req.params.id, function (err, project) {
                if (err) {
                    console.error(err);
                    return res.sendStatus(500);
                }
                if (!project) {
                    console.error("Can't find project" + err);
                    res.sendStatus(304);
                }
                res.sendStatus(200);
            });
    });

module.exports = router;
