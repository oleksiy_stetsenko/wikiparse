var gulp = require('gulp'),
    gulpsync = require('gulp-sync')(gulp),
    del = require('del'),
    uglify = require('gulp-uglify');
// var connect = require('gulp-connect')
// var browserify = require('browserify')
// var source = require('vinyl-source-stream')
// var del = require('del')
// var print = require('gulp-print')

gulp.task('default', ['deploy']);


/// DEPLOY

gulp.task('deploy:clear', function () {
    return del(['deploy/*', '!deploy/.git/']);
});

gulp.task('deploy:css', function () {
    return gulp.src(['./src/public/css/**/*'])
        .pipe(gulp.dest('./deploy/public/css/'));
});

gulp.task('deploy:html', function () {
    return gulp.src(['./src/public/**/*.html'])
        .pipe(gulp.dest('./deploy/public'));
});

gulp.task('deploy:views', function () {
    return gulp.src(['./src/views/**/*'])
        .pipe(gulp.dest('./deploy/views'));
});

gulp.task('deploy:common', function () {
    return gulp.src(['./package*.json'])
        .pipe(gulp.dest('./deploy/'));
});

gulp.task('deploy:js', function () {
    return gulp.src(['./src/**/*.js'])
        // .pipe(uglify())
        .pipe(gulp.dest('./deploy/'));
});

gulp.task('deploy:fonts', function () {
    return gulp.src(['./src/public/fonts/*'])
        .pipe(gulp.dest('./deploy/public/fonts/'));
});

gulp.task('deploy:copy', [
    'deploy:css',
    'deploy:js',
    'deploy:fonts',
    'deploy:html',
    'deploy:views',
    'deploy:common'
]);

gulp.task('deploy', gulpsync.sync(['deploy:clear', ['deploy:copy']]));
