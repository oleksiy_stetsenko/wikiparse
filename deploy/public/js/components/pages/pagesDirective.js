'use strict';

function pages() {
	return {
		controller: 'PagesController',
		controllerAs: 'ctrl',
		bindToController: {
			open: '='
		},
		templateUrl: 'js/components/pages/pages.html'
	};
} // pages

module.exports = pages;
