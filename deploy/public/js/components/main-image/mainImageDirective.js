'use strict';

function mainImage() {
	return {
		controller: 'MainImageController',
		controllerAs: 'mainImgCtrl',
        templateUrl: 'js/components/main-image/main-image.html'
	};
} // mainImage

module.exports = mainImage;
