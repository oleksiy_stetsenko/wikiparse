'use strict';

function images() {
	return {
		controller: 'ImagesController',
		controllerAs: 'ctrl',
        templateUrl: 'js/components/images/images.html'
	};
} // images

module.exports = images;
