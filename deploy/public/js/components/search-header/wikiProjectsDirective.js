'use strict';

function wikiProjects() {
	return {
		controller: 'SearchHeadController',
		controllerAs: 'ctrl',
		templateUrl: 'js/components/search-header/wiki-projects.html'
	};
} // wikiProjects

module.exports = wikiProjects;
