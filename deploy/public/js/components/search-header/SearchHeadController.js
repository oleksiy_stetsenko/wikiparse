function SearchHeadController(ParamService, ProjectsService, LanguagesService, MainService, utils) {
	'use strict';

	LanguagesService.get();
	ProjectsService.get();

	var ctrl = this;
	ctrl.paramService = ParamService;
	ctrl.languageService = LanguagesService;
	ctrl.projectService = ProjectsService;


	/*** PUBLIC METHODS ***/

	ctrl.checkMaxResults = function () {
		if (ParamService.pages.gsrlimit > 50) ParamService.pages.gsrlimit = 50;
	}; // checkMaxResults

	ctrl.isSelectedProject = function (project) {
		return ParamService.settings.domain == project.name;
	}; // isChosenProject

	ctrl.toggleSave = function () {
		ParamService.toggleSave();
	}; // toggleRemember

	ctrl.reset = function () {
		resetSearchTerm();
        MainService.clearResults();
	}; // reset

    ctrl.hardReset = function () {
        ctrl.reset();
        ParamService.turnOffRemember();
        ParamService.resetToDefaults();
    }; // reset

	ctrl.refreshLanguages = function () {
		LanguagesService.get();
	}; // refreshLanguages

	ctrl.refreshProjects = function () {
		ProjectsService.get();
	}; // refreshProjects

	ctrl.isCommons = function () {
		return ParamService.isCommons();
	}; // isCommons

	ctrl.updateProject = function(project) {
		console.log('Saving title ' + project);
		ProjectsService.edit(project);
	};

	ctrl.cancelEdit = function(project) {
		console.log('Canceled editing', project);
	};

	ctrl.removeProject = function (project) {
		console.log("need delete source " + project);
		ProjectsService.remove(project);
		ProjectsService.get(project);
	};

	ctrl.addProject = function (newProject) {
		var temp = {
			name: newProject.name,
			url: newProject.url
		};

		ProjectsService.create(temp);
		ProjectsService.get(temp);
		this.cleanFields();
	};

	ctrl.cleanFields = function () {
		console.log("clean field");
		ctrl.newProject.name = "";
		ctrl.newProject.url = "";
	};

	/*** PRIVATE ***/

	function resetSearchTerm() {
		ParamService.setSearchTerm('');
		utils.resetPath();
	} // resetSearchTerm


} // SearchHeadController

module.exports = SearchHeadController;
