'use strict';


function ProjectsService($http, ParamService) {

    var service = this;
    service.projects = [];

    // var availableProjects = [{
    // 	name: 'wikipedia',
    // 	logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/53px-Wikipedia-logo-v2.svg.png'
    // }, {
    // 	name: 'wikiquote',
    // 	logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Wikiquote-logo.svg/40px-Wikiquote-logo.svg.png'
    // }, {
    // 	name: 'wiktionary',
    // 	logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Wiktionary-logo-en.svg/50px-Wiktionary-logo-en.svg.png'
    // }, {
    // 	name: 'wikisource',
    // 	logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/46px-Wikisource-logo.svg.png'
    // }, {
    // 	name: 'wikivoyage',
    // 	logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Wikivoyage-Logo-v3-icon.svg/48px-Wikivoyage-Logo-v3-icon.svg.png'
    // }, {
    // 	name: 'wikinews',
    // 	logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Wikinews-logo.svg/48px-Wikinews-logo.svg.png'
    // }, {
    // 	name: 'commons',
    // 	logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/36px-Commons-logo.svg.png'
    // }]; // availableProjects


    /*** DATA ***/

    var availableProjects = [];

    $http.get('/source').then(function (item) {
        availableProjects = item.data;
    });

    /*** METHODS ***/

    service.get = function () {
        // console.log(ParamService.languagesUrl);
        $http.jsonp(ParamService.languagesUrl)
            .success(function (data) {
                resetProjects();
                filterProjects(data);
            });
    }; // get

    service.create = function (project) {
        $http.post('/source', project)
            .success(function (data) {
                console.log("source saved" + JSON.stringify(data));
                availableProjects.push(data);
            });
    };

    service.remove = function (project) {
        $http.delete('/source/' + project._id)
            .success(function (data) {
                availableProjects = availableProjects.filter(function (item) {
                    return item._id !== project._id;
                })
            });
    };

    service.edit = function (project) {
        $http.put('/source/' + project.hash, project);
    };

    /*** HELPERS ***/

    function filterProjects() {
        for (var i = 0; i < availableProjects.length; i++) {
            service.projects.push(availableProjects[i]);
        }
    } // filterProjects

    function resetProjects() {
        service.projects = [];
    } // resetProjects

    function isChosenLang(thisLang) {
        return thisLang.code === ParamService.getLang();
    } // isChosenLang


} // ProjectsService

module.exports = ProjectsService;
